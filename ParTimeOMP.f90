program odeParTime20

    use omp_lib
    use mod_time_int
    
    implicit none
    
    integer, parameter :: dp=kind(1.d0)
        real(dp), allocatable :: u(:), c(:), dif(:), u_exact(:)
        real(dp) :: dt, t0, u0, fu1, fu2, dx, k2, norm, err, aux
        integer :: i, j, k, maxIter, T, coarses_num, thread_id, n
        double precision start, end
        character(len=32) :: arg
    
        do i = 1, command_argument_count()
            call get_command_argument(i, arg)
            IF (LEN_TRIM(arg) > 0) then
               read(arg, *) coarses_num !number of coarse divisions
            end if
        end do
    
        !!initialize parameters
        T=1 !!we are on the interval [0,1]
        dt = real(T)/real(coarses_num) !!we divide the time in sub-times T/coarses
        allocate(u(coarses_num+1))
        allocate(c(coarses_num+1))
        allocate(dif(coarses_num+1))
        u=0.0_dp
        c=0.0_dp
        dif=0.0_dp
        n=1e8
        err = 1e-8
    
        !!initial condition
        u0=1.0_dp
        u(1)=u0
        t0=0.0_dp
    
        !!exact solution
        allocate(u_exact(coarses_num+1))
        u_exact(1) = u0
        k2=u0/exp(-t0)
        do i = 2,coarses_num+1
            u_exact(i) = exp(-(t0+dt*real((i-1))))*k2
         end do
  
     !!coarse grid
     do i=1,coarses_num
         call beuler(dt,u(i), c(i))
         u(i+1)=c(i)
 !        print *,c(i)
     end do
 
     maxIter=100
     norm = 1.0_dp !to enter the while
     i=1
 
     start = omp_get_wtime()
     do while (i< maxIter .and. norm>=err)
 
     norm = 0.0_dp
     aux = 0.0_dp
            !!creating a parallel region
                !$omp parallel do shared(n,dif,c, dt, u, coarses_num, dx) private(fu1,fu2,j,k)
                 do k=1,coarses_num
                 fu1=u(k)
                 dx = dt/n
                    do j=1,n
                        call feuler(dx,fu1, fu2)
                        fu1=fu2
                    end do
                 u(k)=fu2
                 dif(k)=u(k)-c(k) 
                 end do
                !$omp end parallel do
    
            u(1)=u0

            !!vector solution update
            do j=1,coarses_num
                call beuler(dt,u(j),c(j))
                u(j+1)=c(j)+dif(j)
            end do
    
             do j=1,coarses_num+1
                  aux = u_exact(j)-u(j)
                  aux = aux*aux
                  norm = norm + aux
             end do
        i=i+1
    
        end do

        end = omp_get_wtime()
    
        !!Printamos tiempo

        write(*, '(f12.8)', advance="no") end - start
    
    end program odeParTime20
