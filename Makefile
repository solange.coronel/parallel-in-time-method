
make_compile:
ifort -O3 -march=native -fopenmp -c mod_time_int.f90 ;
ifort -O3 -march=native -fopenmp -c ParTimeOMP.f90 ;
ifort -O3 -march=native -fopenmp -o ParTimeOMP.x mod_time_int.o ParTimeOMP.o ;