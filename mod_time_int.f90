module mod_time_int
        contains
                subroutine beuler(dt,uold,unew)

                        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                        ! Backwards Euler integration for u,t = -au. Receives time-step size,     !
                        ! u^n and prediction value for u^n+1. Outputs correct value of u^n+1      !
                        ! by means of a Newton-Raphson algorithm.                                 !
                        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

                        implicit none

                        integer, parameter :: dp=kind(1.d0)
                        real(dp), intent(in)    :: dt, uold
                        real(dp), intent(inout) :: unew

                        integer            :: iter, maxIter
                        real(dp)                :: tol, err
                        real(dp)                :: a, r, f, dr, v, T

                        !
                        ! Control params.
                        !
                        maxIter = 100
                        tol = 0.000001d0
                        a = 1.0d0

                        !
                        ! Newton algo.
                        !
                        do iter = 0,maxIter
                           r = -a*unew ! rhs, change if rhs is different
                           dr = -a     ! Derivative dr/dunew, change for different rhs
                           f = unew - dt*r - uold
                           err = sqrt(f*f)
                           if (err .le. tol) then
                        !       write(*,'("*** Solution converged! ***")')
                        !       write(*,'("*** Iterations := ",i0)') iter
                        !       write(*,'("*** Error := ",f16.8)') err
                              exit
                           else
                              T = 1.0d0-dt*dr
                              v = -(1.0d0/T)*f
                              unew = unew+v
                           end if

                        end do
                end subroutine beuler

                subroutine feuler(dt,uold,unew)

                        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                        ! Forward Euler integration for u,t = -au. Receives time-step size and    !
                        ! previous value u^n. Outputs new value u^n+1                             !
                        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

                        implicit none
                        
                        integer, parameter :: dp=kind(1.d0)
                        real(dp), intent(in)  :: dt, uold
                        real(dp), intent(out) :: unew

                        real(dp)              :: a, r

                        !
                        ! Control params.
                        !
                        a = 1.0d0

                        !
                        ! Advance
                        !
                        r = -a*uold
                        unew = uold+dt*r

                end subroutine feuler

end module mod_time_int
