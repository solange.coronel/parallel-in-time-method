
#!/bin/sh
#BSUB -J ParTimeOMP
#BSUB -oo ParTimeOMP_%J.out
#BSUB -eo ParTimeOMP_%J.err
#BSUB -n 64
#BSUB -W 01:00
#BSUB -q debug

initial_coarse=64

echo "starting test"
echo "Threads,t1,t2,t3,t4,t5,t6,t7,t8,t9,t10"
for nt_th in 1 2 4 8 16 32 64
do
echo -n "$nt_th"","
    for times in 1 2 3 4 5 6
    do

        export OMP_NUM_THREADS=${nt_th}
        ./ParTimeOMP.x $((initial_coarse * nt_th ))
        echo -n ","
    done
    printf "\n"
done